interface IGraphTemplate<E> {

    void addVertex(E e);

    void addEdge(double edge, int first, int second);

    void setSize(int size);

    int getSize();

    E getElement(int numElem);

    void printMatrix();

    void destroy();

    boolean isEmpty();

    int getSizeEdge();

    boolean isEdge(int first, int second);

    void deleteVertex(int numThisVertext);
}
