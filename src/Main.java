
import java.util.ArrayList;
import java.util.List;

public class Main {


    public static void main(String[] args) {


        List<Integer> vertex = new ArrayList<>();

        for (int i = 0; i < 3; i++)
            vertex.add(i);

        IGraphTemplate<Integer> graph = new GraphTemplate<>(vertex, vertex.size());
        graph.addEdge(3, 0, 1);
        graph.addEdge(4, 1, 2);
        graph.addEdge(5, 2, 0);
        graph.printMatrix();

        System.out.println("\n Delete vertex = 1");
        graph.deleteVertex(1);
        graph.printMatrix();

        System.out.println("Delete graph");
        graph.destroy();
        graph.printMatrix();


        IGraphTemplate<Integer> graph1 = new GraphTemplate<>(vertex, vertex.size());
        graph1.addEdge(3, 0, 1);
        graph1.addEdge(4, 1, 2);
        graph1.printMatrix();

        System.out.println("Vertex Size = " + graph1.getSize());


        System.out.println("Edge size = " + graph1.getSizeEdge());


        if (graph1.isEdge(0, 1))
            System.out.println("EDGE between 0, 1 - YES");

        if (!graph1.isEdge(2, 0))
            System.out.println("EDGE between 2, 0 - NO");


        System.out.println("Graph addVertex");
        graph1.addVertex(3);
        System.out.println("Vertex Size = " + graph1.getSize());
        graph1.printMatrix();

        System.out.println("Add edge between 0 , 3  = 7 ");
        graph1.addEdge(7, 0, 3);
        graph1.printMatrix();

    }


}
