import java.util.ArrayList;
import java.util.List;

public class GraphTemplate<E> implements IGraphTemplate<E> {

    private List<E> vertex = new ArrayList<>();

    private double[][] matrix;

    private int size;

    private int sizeEdge;


    @Override
    public boolean isEdge(int first, int second) {
        if (matrix[first][second] > 0)
            return true;
        else
            return false;
    }

    @Override
    public void deleteVertex(int numThisVertex) {
        for (int i = 0; i < size; i++)
            for (int j = numThisVertex; j < size; j++)
                if (j == (size - 1))
                    matrix[i][j] = 0;
                else
                    matrix[i][j] = matrix[i][j + 1];

        for (int i = numThisVertex; i < size; i++)
            for (int j = 0; j < size; j++)
                if (i == (size - 1))
                    matrix[i][j] = 0;
                else
                    matrix[i][j] = matrix[i + 1][j];
        size--;
    }

    public GraphTemplate(List<E> vertex, int size) {
        this.vertex = vertex;
        this.size = size;
        matrix = new double[this.size][this.size];

    }

    public GraphTemplate() {
        this.size = 0;
        matrix = new double[0][0];
    }

    @Override
    public void destroy() {
        vertex = null;
        matrix = null;
        size = 0;

    }

    @Override
    public boolean isEmpty() {
        if (size == 0)
            return true;
        else
            return false;

    }

    @Override
    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public E getElement(int numElem) {
        return vertex.get(numElem);
    }

    @Override
    public void addVertex(E e) {
        boolean is = false;
        for (int i = 0; i < vertex.size(); i++)
            if (vertex.get(i) == e) {
                is = true;
                System.out.println("This element is in the graph");
            }

        if (!is) {
            double _matrix[][] = matrix;
            vertex.add(e);
            size++;
            matrix = new double[size][size];
            for (int i = 0; i < _matrix.length; i++)
                System.arraycopy(_matrix[i], 0, matrix[i],
                        0, _matrix.length);
        }

    }

    @Override
    public int getSizeEdge() {
        return sizeEdge;
    }

    @Override
    public void addEdge(double edge, int first, int second) {
        matrix[first][second] = edge;
        matrix[second][first] = edge;
        sizeEdge++;
    }

    @Override
    public void printMatrix() {

        if (!isEmpty())
            for (int i = 0; i < size; i++) {
                System.out.println("\n");
                for (int j = 0; j < size; j++)
                    System.out.printf(matrix[i][j] + "\t");
            }
        else
            System.out.println("Graph is empty");
        System.out.println("\n");
    }
}
